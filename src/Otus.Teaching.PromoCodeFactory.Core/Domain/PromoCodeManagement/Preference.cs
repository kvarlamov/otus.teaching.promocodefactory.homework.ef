﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference
        :BaseEntity
    {
        public string Name { get; set; }
        public List<PromoCode> PromoCodes { get; set; }
        public List<CustomerPreference> CustomerPreferences { get; set; }

        public Preference()
        {
            CustomerPreferences = new List<CustomerPreference>();
            PromoCodes = new List<PromoCode>();
        }
    }
}