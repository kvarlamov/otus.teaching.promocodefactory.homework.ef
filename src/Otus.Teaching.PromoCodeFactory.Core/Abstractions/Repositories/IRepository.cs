﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        
        Task<T> GetByIdAsync(Guid id);
        Task<T> GetByIdIncludePropertyAsync(Guid id, params string[] property);

        Task<Guid> Add(T record);
        Task<T> Update(T record);
        Task<T> Delete(Guid id);
    }
}