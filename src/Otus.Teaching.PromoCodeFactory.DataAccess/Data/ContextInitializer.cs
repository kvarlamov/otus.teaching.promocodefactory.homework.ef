﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class ContextInitializer
    {
        private readonly DbApplicationContext _db;

        public ContextInitializer(DbApplicationContext db)
        {
            _db = db;
        }
        public async Task InitializeAsync()
        {
            var db = _db.Database;
            await db.EnsureDeletedAsync();

            //await db.EnsureCreatedAsync();
            await db.MigrateAsync();
            
            await _db.Roles.AddRangeAsync(FakeDataFactory.Roles);
            await _db.Preferences.AddRangeAsync(FakeDataFactory.Preferences);
            await _db.PromoCodes.AddRangeAsync(FakeDataFactory.PromoCodes);
            await _db.Employees.AddRangeAsync(FakeDataFactory.Employees);
            await _db.Customers.AddRangeAsync(FakeDataFactory.Customers);
            
            await _db.SaveChangesAsync();
        }
    }
}