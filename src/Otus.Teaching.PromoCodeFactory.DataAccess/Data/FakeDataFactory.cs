﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static IEnumerable<Employee> Employees => new List<Employee>()
        {
            new Employee()
            {
                Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                Email = "isergeev@testmail.ru",
                FirstName = "Иван",
                LastName = "Сергеев",
                RoleId = Roles.First(x => x.Name == "Admin").Id,
                AppliedPromocodesCount = 5
            },
            new Employee()
            {
                Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                Email = "andreev@testmail.ru",
                FirstName = "Петр",
                LastName = "Андреев",
                RoleId = Roles.First(x => x.Name == "PartnerManager").Id,
                AppliedPromocodesCount = 10
            },
        };

        public static IEnumerable<Role> Roles => new List<Role>()
        {
            new Role()
            {
                Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                Name = "Admin",
                Description = "Администратор",
            },
            new Role()
            {
                Id = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                Name = "PartnerManager",
                Description = "Партнерский менеджер"
            }
        };
        
        public static IEnumerable<Preference> Preferences => new List<Preference>()
        {
            new Preference()
            {
                Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                Name = "Театр",
            },
            new Preference()
            {
                Id = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                Name = "Семья",
            },
            new Preference()
            {
                Id = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                Name = "Дети",
            }
        };

        public static IEnumerable<PromoCode> PromoCodes
        {
            get
            {
                var codes = new List<PromoCode>()
                {
                    new PromoCode()
                    {
                        Id = Guid.Parse("7f9a020d-4bfc-460e-99b8-a3f94d9aa45a"),
                        Code = "code1",
                        PreferenceId = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                        BeginDate = DateTime.Now,
                        EndDate = DateTime.Now.AddDays(100),
                        PartnerName = "some name",
                        ServiceInfo = "some info",
                        CustomerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0")
                    },
                    new PromoCode()
                    {
                        Id = Guid.Parse("66fe4531-9920-44c5-8865-db420c506952"),
                        Code = "code2",
                        PreferenceId = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                        BeginDate = DateTime.Now,
                        EndDate = DateTime.Now.AddDays(100),
                        PartnerName = "some name2",
                        ServiceInfo = "some info2",
                        CustomerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0")
                    },
                    new PromoCode()
                    {
                        Id = Guid.Parse("e65f7e84-67f4-4be6-bb22-defee4ab4239"),
                        Code = "code3",
                        PreferenceId = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                        BeginDate = DateTime.Now,
                        EndDate = DateTime.Now.AddDays(100),
                        PartnerName = "some name3",
                        ServiceInfo = "some info3",
                        CustomerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f2")
                    }
                };

                return codes;
            }
        }

        public static IEnumerable<Customer> Customers
        {
            get
            {
                var customerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0");
                var customerId2 = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f2");
                var customers = new List<Customer>()
                {
                    new Customer()
                    {
                        Id = customerId,
                        Email = "ivan_sergeev@mail.ru",
                        FirstName = "Иван",
                        LastName = "Петров",
                        // PromoCodes = new List<PromoCode>()
                        // {
                        //     PromoCodes.First(x => x.Id == Guid.Parse("7f9a020d-4bfc-460e-99b8-a3f94d9aa45a")),
                        //     PromoCodes.First(x => x.Id == Guid.Parse("66fe4531-9920-44c5-8865-db420c506952")),
                        // },
                        CustomerPreferences = new List<CustomerPreference>()
                        {
                            new CustomerPreference
                            {
                                PreferenceId = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                                CustomerId = customerId
                            },
                            new CustomerPreference
                            {
                                PreferenceId = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                                CustomerId = customerId
                            }
                        }
                    },
                    new Customer()
                    {
                        Id = customerId2,
                        Email = "sergey_ivanov@mhail.ru",
                        FirstName = "Сергей",
                        LastName = "Иванов",
                        // PromoCodes = new List<PromoCode>()
                        // {
                        //     PromoCodes.First(x => x.Id == Guid.Parse("e65f7e84-67f4-4be6-bb22-defee4ab4239"))
                        // },
                        CustomerPreferences = new List<CustomerPreference>()
                        {
                            new CustomerPreference
                            {
                                PreferenceId = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                                CustomerId = customerId2
                            },
                            new CustomerPreference
                            {
                                PreferenceId = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                                CustomerId = customerId2
                            }
                        }
                    }
                };

                return customers;
            }
        }
    }
}