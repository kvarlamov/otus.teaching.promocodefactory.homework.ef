﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class DbApplicationContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }
        public DbSet<CustomerPreference> CustomerPreferences { get; set; }

        public DbApplicationContext(DbContextOptions<DbApplicationContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Role>(entity =>
            {
                entity.HasOne(p => p.Employee)
                    .WithOne(p => p.Role)
                    .HasForeignKey<Employee>(p => p.RoleId);
                entity.Property(p => p.Id)
                    .ValueGeneratedNever();
                entity.Property(p => p.Name)
                    .HasMaxLength(200);
                entity.Property(p => p.Description)
                    .HasMaxLength(200);
            });
            
            modelBuilder.Entity<Employee>(entity =>
            {
                entity.HasOne(p => p.PromoCode)
                    .WithOne(p => p.PartnerManager)
                    .HasForeignKey<PromoCode>(p => p.EmployeeId);
                entity.Property(p => p.Id)
                    .ValueGeneratedNever();
                entity.Property(p => p.Email)
                    .HasMaxLength(200);
                entity.Property(p => p.FirstName)
                    .HasMaxLength(100);
                entity.Property(p => p.LastName)
                    .HasMaxLength(100);
                entity.Ignore(p => p.FullName);
            });
            
            modelBuilder.Entity<PromoCode>(entity =>
            {
                entity.Property(p => p.Id)
                    .ValueGeneratedNever();
                entity.HasOne(p => p.Customer)
                    .WithMany(p => p.PromoCodes)
                    .OnDelete(DeleteBehavior.Cascade);
                entity.HasOne(p => p.Preference)
                    .WithMany(p => p.PromoCodes);
                entity.Property(p => p.Code)
                    .HasMaxLength(200);
                entity.Property(p => p.PartnerName)
                    .HasMaxLength(200);
                entity.Property(p => p.ServiceInfo)
                    .HasMaxLength(200);
            });
            
            modelBuilder.Entity<Preference>(entity =>
            {
                entity.Property(p => p.Id)
                    .ValueGeneratedNever();
                entity.Property(p => p.Name)
                    .HasMaxLength(200);
            });
            
            modelBuilder.Entity<Customer>(entity =>
            {
                entity.Property(p => p.Id)
                    .ValueGeneratedNever();
                entity.Property(p => p.Email)
                    .HasMaxLength(200);
                entity.Property(p => p.FirstName)
                    .HasMaxLength(100);
                entity.Property(p => p.LastName)
                    .HasMaxLength(100);
                entity.Ignore(p => p.FullName);
            });

            modelBuilder.Entity<CustomerPreference>(entity =>
            {
                entity.HasKey(p => new { p.CustomerId, p.PreferenceId });
                entity.HasOne(p => p.Customer)
                    .WithMany(p => p.CustomerPreferences)
                    .HasForeignKey(p => p.CustomerId);
                entity.HasOne(p => p.Preference)
                    .WithMany(p => p.CustomerPreferences)
                    .HasForeignKey(p => p.PreferenceId);
            });
        }
    }
}