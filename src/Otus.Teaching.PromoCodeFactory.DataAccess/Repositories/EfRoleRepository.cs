﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.Abstractions;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRoleRepository : EfCoreRepository<Role, DbApplicationContext>
    {
        public EfRoleRepository(DbApplicationContext context) : base(context)
        {
            
        }
    }
}