﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.Abstractions;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfPromoCodesRepository : EfCoreRepository<PromoCode, DbApplicationContext>
    {
        public EfPromoCodesRepository(DbApplicationContext db) : base(db)
        {
            
        }
    }
}