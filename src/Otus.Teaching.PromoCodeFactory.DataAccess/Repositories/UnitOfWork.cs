﻿using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DbApplicationContext _db;
        
        public EfCustomerRepository Customers { get;  }
        public EfEmployeeRepository Employees { get; }
        public EfRoleRepository Roles { get; }
        public EfPreferenceRepository Preferences { get; }
        public EfPromoCodesRepository PromoCodes { get; set; }

        public UnitOfWork(DbApplicationContext db)
        {
            _db = db;
            Customers = new EfCustomerRepository(_db);
            Employees = new EfEmployeeRepository(_db);
            Roles = new EfRoleRepository(_db);
            Preferences = new EfPreferenceRepository(_db);
            PromoCodes = new EfPromoCodesRepository(_db);
        }
        
        public async Task Complete()
        {
            await _db.SaveChangesAsync();
        }
        
        public void Dispose()
        {
            _db.Dispose();
        }
    }
}