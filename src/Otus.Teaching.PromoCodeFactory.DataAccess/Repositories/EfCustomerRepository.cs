﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.Abstractions;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfCustomerRepository : EfCoreRepository<Customer, DbApplicationContext>
    {
        private readonly DbApplicationContext _db;

        public EfCustomerRepository(DbApplicationContext db) : base(db)
        {
            _db = db;
        }
        
        public async Task<Customer> GetByIdIncludePreferencesAsync(Guid id)
        {
            return await _db.Customers.Where(x => x.Id == id)
                .Include(cp => cp.CustomerPreferences)
                .ThenInclude(p => p.Preference)
                .Include(p => p.PromoCodes).FirstOrDefaultAsync();
        }

        public async Task<IList<Customer>> GetCustomersIncludePreferencesByIdAsync(Guid preferenceId)
        {
            return await _db.Customers
                .Include(cp => cp.CustomerPreferences)
                .ThenInclude(p => p.Preference)
                .Where(p => p.CustomerPreferences.Any(x => x.PreferenceId == preferenceId))
                .Include(p => p.PromoCodes).ToListAsync();
        }
    }
}