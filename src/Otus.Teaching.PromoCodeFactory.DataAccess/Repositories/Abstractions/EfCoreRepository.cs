﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.Abstractions
{
    public abstract class EfCoreRepository<TEntity, TContext> : IRepository<TEntity> 
        where TEntity: BaseEntity
        where TContext: DbContext
    {
        private readonly TContext _db;
        public EfCoreRepository(TContext db)
        {
            _db = db;
        }
        public async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            return await _db.Set<TEntity>().ToListAsync();
        }

        public async Task<TEntity> GetByIdIncludePropertyAsync(Guid id, params string[] property)
        {
            var query = _db.Set<TEntity>();
            foreach (var param in property)
            {
                query.Include(param);
            }
            
            return await query.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<TEntity> GetByIdAsync(Guid id)
        {
            return await _db.Set<TEntity>().FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<Guid> Add(TEntity record)
        {
            _db.Set<TEntity>().Add(record);
            return record.Id;
        }

        public async Task<TEntity> Update(TEntity record)
        {
            _db.Entry(record).State = EntityState.Modified;
            return record;
        }

        public async Task<TEntity> Delete(Guid id)
        {
            var entity = await _db.Set<TEntity>().FindAsync(id);
            if (entity == null)
            {
                throw new Exception("Not found");
            }

            _db.Set<TEntity>().Remove(entity);
            return entity;
        }
    }
}