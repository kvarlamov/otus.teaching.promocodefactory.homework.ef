﻿using System;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public interface IUnitOfWork : IDisposable
    {
        EfCustomerRepository Customers { get; }
        EfEmployeeRepository Employees { get; }
        EfRoleRepository Roles { get; }
        EfPreferenceRepository Preferences { get; }
        EfPromoCodesRepository PromoCodes { get; set; }
        Task Complete();
    }
}