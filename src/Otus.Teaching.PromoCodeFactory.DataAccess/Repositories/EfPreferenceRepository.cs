﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.Abstractions;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfPreferenceRepository : EfCoreRepository<Preference, DbApplicationContext>
    {
        public EfPreferenceRepository(DbApplicationContext context) : base(context)
        {
            
        }
    }
}