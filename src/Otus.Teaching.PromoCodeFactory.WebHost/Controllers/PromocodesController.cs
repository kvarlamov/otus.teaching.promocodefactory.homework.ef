﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        public PromocodesController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            //TODO: Получить все промокоды 
            var promoCodes = await _unitOfWork.PromoCodes.GetAllAsync();
            var response = promoCodes.Select(x => new PromoCodeShortResponse
            {
                Id = x.Id,
                Code = x.Code,
                BeginDate = x.BeginDate.ToString(CultureInfo.InvariantCulture),
                EndDate = x.EndDate.ToString(CultureInfo.InvariantCulture),
                PartnerName = x.PartnerName,
                ServiceInfo = x.ServiceInfo
            });

            return Ok(response);
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            //TODO: Создать промокод и выдать его клиентам с указанным предпочтением
            if (request == null)
                return BadRequest();

            if (!Guid.TryParse(request.Preference, out Guid preferenceGuid))
                return BadRequest("Preference Guid isn't correct");

            var preference = await _unitOfWork.Preferences.GetByIdAsync(preferenceGuid);
            if (preference == null)
                return BadRequest("Preference doesn't exitst");

            var newPromo = new PromoCode()
            {
                Id = Guid.NewGuid(),
                Code = request.PromoCode,
                PreferenceId = preference.Id,
                PartnerName = request.PartnerName,
                ServiceInfo = request.ServiceInfo
            };

            await _unitOfWork.PromoCodes.Add(newPromo);

            var customers = await _unitOfWork.Customers.GetCustomersIncludePreferencesByIdAsync(preference.Id);
            foreach (var customer in customers)
            {
                customer.PromoCodes.Add(newPromo);
            }
            
            await _unitOfWork.Complete();
            return Ok();
        }
    }
}