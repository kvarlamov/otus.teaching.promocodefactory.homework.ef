﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NJsonSchema;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        public CustomersController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        
        /// <summary>
        /// Get all customers
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IList<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _unitOfWork.Customers.GetAllAsync();
            List<CustomerShortResponse> result = customers.Select(x =>
                new CustomerShortResponse
                {
                    Id = x.Id,
                    Email = x.Email,
                    FirstName = x.FirstName,
                    LastName = x.LastName
                }).ToList();
            
            return result;
        }
        
        /// <summary>
        /// Get customer by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            //var customer = await _unitOfWork.Customers.GetByIdIncludePropertyAsync(id, "CustomerPreferences", "CustomerPreference.Preference" , "PromoCodes");
            var customer = await _unitOfWork.Customers.GetByIdIncludePreferencesAsync(id);
            if (customer == null)
                return NotFound();

            var result = new CustomerResponse
            {
                Id = customer.Id,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                PromoCodes = customer.PromoCodes.Select(x => new PromoCodeShortResponse
                {
                    Id = x.Id,
                    Code = x.Code,
                    BeginDate = x.BeginDate.ToString(CultureInfo.InvariantCulture),
                    EndDate = x.EndDate.ToString(CultureInfo.InvariantCulture),
                    PartnerName = x.PartnerName,
                    ServiceInfo = x.ServiceInfo
                }).ToList(),
                Preferences = customer.CustomerPreferences.Select(x => new PreferenceResponse(){Name = x.Preference.Name, Id = x.Preference.Id}).ToList()
            };

            return result;
        }
        
        /// <summary>
        /// Add new customer
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            //TODO: Добавить создание нового клиента вместе с его предпочтениями
            if (request is null)
                throw new ArgumentNullException();

            var preferences = await GetPreferences(request);
            var customerId = Guid.NewGuid();
            var customerPreference = preferences.Select(x => new CustomerPreference()
                { CustomerId = customerId, PreferenceId = x.Id }).ToList();

            var newCustomer = new Customer()
            {
                Id = customerId,
                Email = request.Email,
                LastName = request.LastName,
                FirstName = request.FirstName,
                CustomerPreferences = customerPreference
            };

            await _unitOfWork.Customers.Add(newCustomer);
            return Ok(customerId);
        }

        /// <summary>
        /// Edit Customer
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            //TODO: Обновить данные клиента вместе с его предпочтениями
            if (request == null)
                return BadRequest();
            
            var customer = await _unitOfWork.Customers.GetByIdIncludePreferencesAsync(id);
            if (customer == null)
                return NotFound();

            customer.FirstName = request.FirstName ?? customer.FirstName;
            customer.LastName = request.LastName ?? customer.LastName;
            customer.Email = request.Email ?? customer.Email;
            var preferences = await GetPreferences(request);
            if (preferences.Any())
            {
                customer.CustomerPreferences.Clear();
                foreach (var preference in preferences)
                {
                    customer.CustomerPreferences.Add(new CustomerPreference()
                    {
                        CustomerId = customer.Id,
                        PreferenceId = preference.Id
                    });
                }
            }

            await _unitOfWork.Customers.Update(customer);
            await _unitOfWork.Complete();
            return Ok("updated");
        }
        
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            //TODO: Удаление клиента вместе с выданными ему промокодами
            try
            {
                var entity =  await _unitOfWork.Customers.Delete(id);
                await _unitOfWork.Complete();
                return Ok(entity);
            }
            catch (Exception e)
            {
                return BadRequest($"Customer with ID {id} Not Found");
            }
        }
        
        private async Task<List<Preference>> GetPreferences(CreateOrEditCustomerRequest request)
        {
            var preferences = new List<Preference>();
            if (request.PreferenceIds.Any())
            {
                foreach (var p in request.PreferenceIds)
                {
                    var preference = await _unitOfWork.Preferences.GetByIdAsync(p);
                    if (preference != null)
                    {
                        preferences.Add(preference);
                    }
                }
            }

            return preferences;
        }
    }
}