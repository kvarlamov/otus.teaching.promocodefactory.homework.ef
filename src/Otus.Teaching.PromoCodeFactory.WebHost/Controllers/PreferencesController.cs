﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Preferences
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController
        : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        public PreferencesController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Get all preferences
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IList<PreferenceResponse>> GetPreferencesAsync()
        {
            var preferences = await _unitOfWork.Preferences.GetAllAsync();
            var response = preferences.Select(x => new PreferenceResponse
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();

            return response;
        }
    }
}